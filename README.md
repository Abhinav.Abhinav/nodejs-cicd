# Project: CI/CD Pipeline Overview

This document outlines the Continuous Integration/Continuous Deployment (CI/CD) pipeline configuration for the project, as defined in gitlab-ci.yml file. The pipeline automates the process of testing, building, and deploying the application, ensuring a streamlined development lifecycle and robust application delivery.

## Pipeline Stages
Our CI/CD pipeline is structured into the following stages:

## Test: 
This stage includes automated testing to ensure code integrity. It encompasses:
- Unit testing with coverage report generation.
- Static Application Security Testing (SAST) and Secret Detection using GitLab's predefined templates.
- Snyk Scan: Executes Software Composition Analysis (SCA) to identify known vulnerabilities within project dependencies.

## Build & Push: 
Builds the Docker image of the application, scans it for vulnerabilities with Trivy, and pushes the image to the container registry if no critical issues are found.

## Deploy: 
Deploys the application to a development environment using Docker Compose.

## OWASP ZAP DAST: 
Performs Dynamic Application Security Testing (DAST) using OWASP ZAP to identify runtime vulnerabilities.

## Key Configurations
### Image Naming: 
Images are tagged with the version extracted from package.json and the CI pipeline ID.
### Trivy Scans: 
Critical and high severity vulnerabilities are checked, and the build will fail if such vulnerabilities exist.
### Deployment: 
Utilizes SSH to securely deploy the Docker image to the development server.
### DAST: 
Post-deployment, a baseline security scan is run against the live application endpoint.

## Variables
Certain actions within the pipeline require predefined variables. These include:
### CI_REGISTRY_IMAGE: 
Base name for the Docker image.
### DEV_SERVER_HOST: 
Hostname of the development server.
### DEV_ENDPOINT: 
URL of the deployed application in the development environment.
### SNYK_TOKEN: 
Token for Snyk scans.
### SSH_PRIVATE_KEY: 
Private key for SSH access to the development server.

## Artifacts
The pipeline generates several artifacts that are stored and available for review:
- JUnit reports from unit testing.
- Trivy vulnerability scan report.
- OWASP ZAP DAST report.


# Integrating Snyk with GitLab
This guide describes how to integrate Snyk, a powerful vulnerability scanner, into your GitLab CI/CD pipeline to automatically detect and report security vulnerabilities in your project's dependencies.

# Prerequisites
- A Snyk account. If you don't have one, sign up at Snyk.io.
- Your project's source code hosted on GitLab.
- A GitLab CI/CD pipeline configured in your project (.gitlab-ci.yml file).

# Step 1: 
- Obtain Your Snyk API Token
- Log in to your Snyk account.
- Navigate to your account settings to find your API token.
- Copy your API token; you'll need this to authenticate your GitLab pipeline with Snyk.

# Step 2: 
- Add Snyk API Token to GitLab CI/CD Variables
- Go to your GitLab project.
- Navigate to Settings > CI/CD and expand the Variables section.
- Add a new variable:
    - Key: SNYK_TOKEN
    - Value: Paste your Snyk API token here.
    - Ensure the Mask variable option is selected for security.

# Step 3: Configure Your .gitlab-ci.yml File
Add a job to your .gitlab-ci.yml file to run Snyk tests as mentioned in the pipeline. 

# Step 4: Review Vulnerability Reports
After the pipeline runs, Snyk generates a report detailing any found vulnerabilities. You can view these directly in your GitLab CI/CD pipeline job's output. For a more detailed analysis, review the findings on your Snyk dashboard.

# Integrating SAST and Secret Detection in GitLab
Enhance your project's security posture by integrating GitLab's built-in Static Application Security Testing (SAST) and Secret Detection capabilities into your CI/CD pipeline. This guide will walk you through the steps to set up these security checks, helping you to identify and mitigate vulnerabilities and unintended secret exposures early in the development process.

## Prerequisites
- A project hosted on GitLab with CI/CD enabled.
- A .gitlab-ci.yml file in the root of your repository to define your CI/CD pipeline configurations.

## Step 1: 
- Include SAST and Secret Detection Templates
- GitLab provides predefined templates for SAST and Secret Detection. You can include these templates in your .gitlab-ci.yml file to easily integrate these security checks into your pipeline.

## SAST Integration
To include the SAST template, add the following line to your .gitlab-ci.yml file:
    include:
        - template: Security/SAST.gitlab-ci.yml

This line includes the default SAST template provided by GitLab, which will automatically run SAST jobs in your pipeline.

## Secret Detection Integration
Similarly, to integrate Secret Detection, add the following line:
    include:
        - template: Security/Secret-Detection.gitlab-ci.yml

This will include the Secret Detection template, enabling your pipeline to scan your codebase for secrets that may have been accidentally committed.

## Step 2: Configure Your Pipeline
After including the templates, your pipeline will automatically run SAST and Secret Detection jobs. You can further customize these jobs or add additional configurations as needed. For example, you might want to run these jobs only for specific branches or add custom rules.

## Step 3: Review the Reports
Once the pipeline executes, SAST and Secret Detection jobs will generate reports that detail any findings. You can view these reports in the 'Security & Compliance' > 'Security Dashboard' section of your GitLab project.

## SAST Report: 
Shows vulnerabilities in your codebase, categorized by severity. You can use this report to identify and address potential vulnerabilities.
Secret Detection Report: Lists detected secrets in your repository. Review this report to ensure that no sensitive information is exposed.
